// @flow weak

import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import ButtonBase from 'material-ui/ButtonBase';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Radio, { RadioGroup } from 'material-ui/Radio';
import { FormControl, FormControlLabel } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import MobileStepper from 'material-ui/MobileStepper';
import KeyboardArrowLeft from 'material-ui-icons/KeyboardArrowLeft';
import KeyboardArrowRight from 'material-ui-icons/KeyboardArrowRight';

import { styles } from './style.js';

import { PER_PAGE, LARGE, MEDIUM_1, MEDIUM_2, SMALL } from './const.js';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      gallery: {
        photos: []
      },
      chooseImage: false,
      src: "",
      title: "",
      author: "",
      filterSize: "all",
      filterAuthor: "",
      activeStep: 0,
    };

    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.renderChooseImage = this.renderChooseImage.bind(this);
    this.changeSize = this.changeSize.bind(this);
    this.changeAuthor = this.changeAuthor.bind(this);
    this.renderPhotos = this.renderPhotos.bind(this);
    this.getPhotos = this.getPhotos.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleBack = this.handleBack.bind(this);
  }

  componentDidMount() {
    axios({
      method: 'get',
      url: '/images.json',
      responseType: 'json'
    }).then(response => {
      this.setState({
        gallery: response.data,
      });
    });
  }

  handleRequestClose() {
    this.setState({
      chooseImage: false
    });
  };

  handleRequestOpen(id) {
    const findImage = this.state.gallery.photos.find(photo => photo.id === id)
    this.setState({
      chooseImage: true,
      src: findImage.image_url,
      title: findImage.name
    });
  };

  handleNext() {
    this.setState({
      activeStep: this.state.activeStep + 1,
    });
  };

  handleBack() {
    this.setState({
      activeStep: this.state.activeStep - 1,
    });
  };

  changeSize(event, value) {
    this.setState({
      filterSize: value,
      activeStep: 0,
    });
  };

  changeAuthor(event) {
    this.setState({
      author: event.target.value,
      activeStep: 0
    })
  };

  renderChooseImage() {
    return (
      <div>
        <Dialog open={this.state.chooseImage}
          transition={Slide}
          onRequestClose={this.handleRequestClose}>
          <DialogTitle>
            {this.state.title}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <img src={this.state.src}
                alt={this.state.title}
                style={{
                  width: 500,
                  height: 400
                }} />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleRequestClose} color="primary">
              Cancel
          </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }

  getPhotos() {
    let filterByAuthor = (photo) => {
      if (this.state.author === "") {
        return true;
      }
      else {
        return photo.author.search(new RegExp(this.state.author, "i")) !== -1;
      }
    }
    let filterBySize = (photo) => {
      switch (this.state.filterSize) {
        case "large":
          return function (photo) { return photo.width >= 1920 };
        case "medium":
          return function (photo) { return photo.width >= 1000 && photo.width < 1920 };
        case "small":
          return function (photo) { return photo.width < 1000 };
        case "all":
        default:
          return function (photo) { return true };
      }
    }

    return this.state.gallery.photos
      .filter(filterByAuthor)
      .filter(filterBySize())
  }

  renderPhotos() {
    const classes = this.props.classes;
    let startImage = this.state.activeStep * PER_PAGE;
    let endImage = startImage + PER_PAGE;
    return (
      <div>
        {this.getPhotos().slice(startImage, endImage).map(photo =>
          <ButtonBase
            focusRipple
            key={photo.id}
            onClick={this.handleRequestOpen.bind(this, photo.id)}
            className={classes.image}
          >
            <div
              className={classes.imageSrc}
              style={{
                backgroundImage: `url(${photo.image_url})`,
              }}
            />
            <div className={classes.imageBackdrop} />
            <div className={classes.imageButton}>
              <Typography
                component="h3"
                type="subheading"
                color="inherit"
                className={classes.imageTitle}
              >
                {photo.name} by: {photo.author}
              </Typography>
            </div>
          </ButtonBase>,
        )}
      </div>
    )
  }

  render() {
    const classes = this.props.classes;
    const pagesCount = Math.ceil(this.getPhotos().length / PER_PAGE);
    return (
      <div className={classes.container}>
        <Paper className={classes.paper}>
          <div>
            <Typography type="display3"
              align="left"
              gutterBottom>
              Image view
              </Typography>
          </div>
          <Grid container spacing={24}>
            <Grid item xs={9}>
              <div className={classes.renderPhotos}>
                {this.renderPhotos()}
              </div>
              <div style={{
                marginTop: 10
              }}>
                <Typography
                  type="title">
                  {this.state.activeStep + 1} of {pagesCount}
                </Typography>
              </div>
              <div className={classes.steper}>
                <MobileStepper
                  type="text"
                  steps={pagesCount}
                  position="static"
                  activeStep={this.state.activeStep}
                  className={classes.mobileStepper}
                  nextButton={
                    <Button dense
                      onClick={this.handleNext}
                      disabled={this.state.activeStep === pagesCount - 1}>
                      Next
                    <KeyboardArrowRight />
                    </Button>
                  }
                  backButton={
                    <Button dense
                      onClick={this.handleBack}
                      disabled={this.state.activeStep === 0}>
                      <KeyboardArrowLeft />
                      Back
                  </Button>
                  }
                />
              </div>
              <div>{this.renderChooseImage()}</div>
            </Grid>
            <Grid item xs={3}>
              <div>
                <Typography type="display1" align="left" gutterBottom>
                  Filter By
              </Typography>
              </div>
              <div
                style={{
                  marginRight: 150,
                  marginTop: 50
                }}>
                <Typography type="title" align="left" gutterBottom>
                  Size
                </Typography>
                <FormControl>
                  <RadioGroup
                    aria-label="size"
                    value={this.state.filterSize}
                    onChange={this.changeSize}
                  >
                    <FormControlLabel value="all"
                      control={<Radio />}
                      label="All" />
                    <FormControlLabel value="large"
                      control={<Radio />}
                      label="Large" />
                    <FormControlLabel value="medium"
                      control={<Radio />}
                      label="Medium" />
                    <FormControlLabel value="small"
                      control={<Radio />}
                      label="Small" />
                  </RadioGroup>
                </FormControl>
              </div>
              <div style={{
                marginTop: 50
              }}>
                <Typography type="title" align="left" gutterBottom>
                  Author
                </Typography>
                <form className={classes.formContainer} autoComplete="off">
                  <div>
                    <TextField type="text"
                      name="name"
                      label="Enter the author's name"
                      value={this.state.author}
                      onChange={this.changeAuthor} />
                  </div>
                </form>
              </div>
            </Grid>
          </Grid>
        </Paper>
      </div >
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
