export const PER_PAGE = 20;
export const LARGE = 1500;
export const MEDIUM_1 = 1499;
export const MEDIUM_2 = 800;
export const SMALL = 799; 